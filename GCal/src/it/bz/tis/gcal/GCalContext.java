/*
 * GCal
 * Copyright (C) 2011 Patrick Ohnewein
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WAllerheiligenITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package it.bz.tis.gcal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Context object.
 * @author Patrick Ohnewein
 */
public class GCalContext {
	
	private static final String CONTEXT_NAME = "gcal.context";
	
	private String username;
	private String password;
	private String searchString;
	
	private GCalContext() {
		super();
	}

	public static GCalContext getInstance(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		GCalContext context = (GCalContext)session.getAttribute(CONTEXT_NAME);
		if (context == null) {
			context = new GCalContext();
			session.setAttribute(CONTEXT_NAME, context);
		}
		return context;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
}
