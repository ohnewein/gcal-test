<%@page import="it.bz.tis.gcal.GCalContext"%>

<div id="selectstocksymbol">
<%
{	// start own name space
	
	String username = GCalContext.getInstance(request).getUsername();
	String password = GCalContext.getInstance(request).getPassword();
	String searchString = GCalContext.getInstance(request).getSearchString();
%>
<form action="SetParameters" method="post">
	<div>
      	<span>Unsername:</span><input type="text" name="username" id="username" size="30" maxlength="30"
<%if (username != null) { %> 
      	value="<%=username %>"
<%} %> 
      	/>
	</div>
	<div>
      	<span>Password:</span><input type="password" name="password" id="password" size="30" maxlength="30"
<%if (password != null) { %> 
      	value="<%=password %>"
<%} %> 
      	/>
	</div>
	<div>
      	<span>Search String:</span><input type="text" name="searchstring" id="searchstring" size="50" maxlength="50"
<%if (searchString != null) { %> 
      	value="<%=searchString %>"
<%} %> 
      	/>
	</div>
	<div>
		<input type="submit" value="Submit"/>
	</div>
</form>
<%} %>
</div>
