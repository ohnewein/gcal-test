
<%@page import="com.google.gdata.data.calendar.CalendarFeed"%>
<%@page import="it.bz.tis.gcal.GCalService"%>
<%@page import="it.bz.tis.gcal.GCalContext"%>
<%@page import="com.google.gdata.data.calendar.CalendarEventFeed"%>
<%@page import="com.google.gdata.data.calendar.CalendarEventEntry"%>

<%
{	// start own name space
	GCalService service = GCalService.getInstance(GCalContext.getInstance(request));
	if (service != null) {
		CalendarEventFeed resultFeed = service.getCalendarEvents(GCalContext.getInstance(request).getSearchString());
		if (resultFeed != null) {
%>
			


			<div><h1>Search Results</h1>
			<ul>
<%
			for (CalendarEventEntry entry : resultFeed.getEntries() ) {
			    if (entry != null) {
%>
					<li>
					<%=entry.getTitle().getPlainText() %>
					</li>
<%
			    }
			}
%>
			</ul>
			</div>
<%
		}
	}

}
%>