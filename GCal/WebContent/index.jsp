<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GCal - Google Calendar Analyser</title>
</head>
<body>

<%@ include file="modules/setparameters.jsp" %>
<%@ include file="modules/showcalendars.jsp" %>
<%@ include file="modules/showsearchresult.jsp" %>

</body>
</html>